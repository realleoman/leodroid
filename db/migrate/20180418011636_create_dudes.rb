class CreateDudes < ActiveRecord::Migration
  def change
    create_table :dudes do |t|
      t.text :name
      t.text :last_name
      t.text :age

      t.timestamps
    end
  end
end
